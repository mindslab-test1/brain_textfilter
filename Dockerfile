# Dockerfile for base image (use docker exec or a shell on the container)
FROM nvcr.io/nvidia/pytorch:20.07-py3

RUN apt-get update && apt-get install -y vim curl && \
    bash <(curl -s https://raw.githubusercontent.com/konlpy/konlpy/master/scripts/mecab.sh) &&\
	python3 -m pip --no-cache-dir install --upgrade \
        grpcio grpcio-tools==1.20.1 \
        tqdm nltk konlpy langdetect omegaconf sklearn transformers koco && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
	ldconfig && \
	apt-get clean && \
	apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/*

ARG WORKSPACE=/pef

RUN mkdir ${WORKSPACE}
COPY . ${WORKSPACE}

VOLUME ${WORKSPACE}/models

WORKDIR ${WORKSPACE}

EXPOSE 35022

ENV MODEL_PATH=/pretrained/text_filter/
ENV PORT=35022

ENTRYPOINT python src/server.py --port $PORT --model_path $MODEL_PATH
