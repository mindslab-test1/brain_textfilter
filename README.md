Text Fliter
===

BERT 기반의 문장 필터링 모델
- version: 0.9.0
- 현재 server 용 inference 만 허용함.

#### * clone 시 `git clone --recursive {address}`를 통해 submodule까지 받도록 한다


## Docker how-to
1. server 전용 도커 이미지 사용
    - 아래 명령어로 도커 실행
    ```
    docker run -it \
        --gpus device={gpu_device} \
        --restart=always \
        -v {host_pretrained_dir}:/pretrained \
        -p {host_port}:35022 \
        --name text_filter_engine \
        docker.maum.ai:443/brain/text_filter:{version}
    ```
    - 이후 아래 설명에 따라 진행

---
 
## Inference(bulk test, inference, serving) 공통
1. 실행할 때는 `scripts/run_{infer,server, client}.sh` 사용
    - 주요 argument는 `MODEL_PATH`이며, 필요한 config 파일, vocab 등은 해당 경로에서 모두 불러온다.
    - `MODEL_PATH` 는 `/pretrained/text_filter/` 로 고정한다. 


### Inference
1. Inference script 수정 후 실행 (`scripts/run_infer.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - `--infer_text "text" \`: 확인하고자 하는 텍스트 추가(여러개를 추가할 수 있다)
    - `$ bash scripts/run_infer.sh`로 실행


### Serving
1. Server script 수정 후 실행 (`scripts/run_server.sh`)
    - `export CUDA_VISIBLE_DEVICES=`: 사용할 GPU 번호(주석으로 처리하면 모든 GPU 사용)
    - `MODEL_PATH=`: 모델 경로
    - `PORT=`: 35022 (내부에서 접근하는 port number - default 값은 35022 임)
    - `$ bash scripts/run_server.sh`로 실행


### Client Demo (Python)
1. `docker exec -it text_filter_engine /bin/bash` 로 컨테이너 접근
2. Client script 수정 후 실행 (`scripts/run_client.sh`)
    - `export CUDA_VISIBLE_DEVICES=` : 사용하는 gpu 번호
    - `PORT=`: 35022 (내부에서 접근하는 port number - default 값은 35022 임)
    - `--passage "text"`: 확인하고자 하는 문장(한 문장) 입력
    - `$ bash scripts/run_client.sh`로 실행

### Troubleshoot
1. 테스트 데이터를 수정 후 업데이트 되지 않을 때,
    - `{data_dir}/wordpiece/test` 데이터 위치한 폴더에 자동으로 생성된 파일을 지우고 다시 실행
2. `$ bash scripts/{}.sh` 실행이 안될 때,
    - 인자를 입력하는 부분에서 `\`뒤에 공백이 없도록 주의
