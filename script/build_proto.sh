#!/bin/bash
PROTO_PARENT='src'
python -m grpc_tools.protoc --proto_path ${PROTO_PARENT} \
    ${PROTO_PARENT}/proto/HateFilter.proto \
    --python_out ${PROTO_PARENT} \
    --grpc_python_out ${PROTO_PARENT} \

// python -m grpc_tools.protoc -Isrc/proto --python_out=src/proto/ --grpc_python_out=src/proto/ src/proto/pef.proto
// https://grpc.io/docs/languages/python/basics/

