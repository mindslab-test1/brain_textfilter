#!/bin/bash

# Generate proto
# bash script/run_build_proto.sh

export CUDA_VISIBLE_DEVICES=3

IP="localhost"
PORT=35022

python3 src/client.py \
    --ip ${IP} \
    --port ${PORT} \
    --passage "최고의 영화. 멍청한 감독! " \

