#!/bin/bash
# export CUDA_VISIBLE_DEVICES=0

MODEL_PATH="base_model"

python src/runner.py --run infer \
    --model_path ${MODEL_PATH} \
    --infer_text "이 영화 진짜 재미 없다" \
    --infer_text "최고의 영화" \
    --infer_text "훌륭한 감독. 반전 영화" \

