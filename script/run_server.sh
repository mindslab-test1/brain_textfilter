#!/bin/bash

# Generate proto
# bash script/run_build_proto.sh

export CUDA_VISIBLE_DEVICES=3

MODEL_PATH="/pretrained/text_filter/"
PORT=35022

python3 src/server.py \
    --model_path ${MODEL_PATH} \
    --port ${PORT} \
