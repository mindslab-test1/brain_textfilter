#!/bin/bash
#export CUDA_VISIBLE_DEVICES=0

MODEL_PATH="models/koco_kcbert"

python src/runner.py --run test \
    --model_path ${MODEL_PATH} \
