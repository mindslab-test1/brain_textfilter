import os, logging
import numpy as np

import torch
# from torch.utils.data import Dataset
from torch.utils.data import Dataset, DataLoader

from konlpy.tag import Mecab
from transformers import BertTokenizer
logger = logging.getLogger(__name__)


class kocodataset(Dataset):
    def __init__(self, max_length, tokenizer, title=True, evaluate=False, binary=False, test=False):
        # self.cfg = cfg
        # TODO max_length 를... cfg.train에서 못 받아온다;; 뭐임;; koco 는 cfg를 다 받아올 필요가 없네?? 
        self.max_length = max_length
        self.mecab = Mecab()
        self.tokenizer = tokenizer
        self.title = title
        self.evaluate = evaluate
        self.binary = binary

        import koco
        if test:
            self.koco_dataset = koco.load_dataset('korean-hate-speech', mode='test')
        else:
            koco_load = koco.load_dataset('korean-hate-speech', mode='train_dev')
            self.koco_dataset = koco_load['dev'] if evaluate else koco_load['train']
        # label encoding
        if binary:
            self.hate_encode = {'none': 0, 'hate':1}
            self.bias_encode = {'none': 0, 'bias':1}
        else:
            self.hate_encode = {'none':0, 'hate':1, 'offensive':2}
            self.bias_encode = {'none':0, 'gender':1, 'others':2}

    def __len__(self):
        return len(self.koco_dataset)

    def __getitem__(self, idx):
        koco_data = self.koco_dataset[idx]

        # x = [cls] news_title [sep] comments [sep]
        _news_title = koco_data['news_title']
        news_title = " ".join(self.mecab.morphs(_news_title))
        _comment = koco_data['comments']
        comment = " ".join(self.mecab.morphs(_comment))
        try:
            contain_gender_bias = koco_data['contain_gender_bias']
            if self.binary:
                bias = self.bias_encode['none' if koco_data['bias']=='none' else 'bias']
                hate = self.hate_encode['none' if koco_data['hate']=='none' else 'hate']
            else:
                bias = self.bias_encode[koco_data['bias']]
                hate = self.hate_encode[koco_data['hate']]
        except KeyError:
            contain_gender_bias = -1
            bias = -1
            hate = -1

        if self.title:
            x = [_news_title, _comment]
        else:
            x = _comment

        # TODO 하나의 label만 들어왔을 때도 사용할 수 있도록
        token = self.tokenizer(text=x, padding='max_length', max_length=self.max_length) \
            if type(x) == str \
            else self.tokenizer(text=x[0], text_pair=x[1], padding='max_length', max_length=self.max_length)
        y = {'contain_gender_bias':[1] if contain_gender_bias==True else [0],
             'bias':[bias],
             'hate':[hate]} # True : 0 False: 1

        dataset = {'sentence': x,
                   'input':token,
                   'targets':y,
                   'seqlen':sum(token.attention_mask)}
        return dataset

def pad(batch):
    '''
    pad 하는 코드가 아님
    '''
    f = lambda x: [sample[x] for sample in batch]
    sentences = f('sentence')
    inputs = f('input')
    targets = f('targets')
    seqlen = f('seqlen')
    
    f = lambda x, which: [sample[x] for sample in which]
    return_input_ids =f('input_ids', inputs)
    return_token_type_ids =f('token_type_ids', inputs)
    return_attention_mask=f('attention_mask', inputs)
    inputs = {
        'input_ids':return_input_ids,
        'token_type_ids':return_token_type_ids,
        'attention_mask':return_attention_mask
    }

    return_contain_gender_bias=f('contain_gender_bias', targets)
    return_bias=f('bias', targets)
    return_hate=f('hate', targets)
    targets = {
            'contain_gender_bias':return_contain_gender_bias,
            'bias':return_bias,
            'hate':return_hate
            }

    maxlen = np.array(seqlen).max()

    return sentences, inputs, targets, seqlen

class KocoDataProcessor():
    def __init__(self, cfg, tokenizer, binary=False):
        self.cfg = cfg
        self.max_length = cfg.model.max_seq_length
        self.batch_size = cfg.train.batch_size
        self.eval_batch_size = cfg.train.eval_batch_size
        self.mecab = Mecab()
        self.tokenizer = tokenizer
        self.title = cfg.train.title
        self.binary = cfg.train.binary

        self._dataset = kocodataset(self.max_length, tokenizer, title=self.title, evaluate=False, binary=self.binary)
        
        # label2idx
        contain_gender_bias_encode = {'True':1, 'False':0}
        hate_encode = self._dataset.hate_encode
        bias_encode = self._dataset.bias_encode
        self.label2idx = (contain_gender_bias_encode, bias_encode, hate_encode)
        
        #idx2label
        idx2contain_gender_bias = {idx: label for label, idx in contain_gender_bias_encode.items()}
        idx2hate = {idx: label for label, idx in hate_encode.items()}
        idx2bias = {idx: label for label, idx in bias_encode.items()}
        self.idx2label = (idx2contain_gender_bias, idx2bias, idx2hate)

        # calculate class weight
        kc = self._dataset.koco_dataset
        contain_gender_bias_labels = [kc[i]['contain_gender_bias'] for i in range(len(kc))]
        contain_gender_bias_weight = self._calculate_class_weight(contain_gender_bias_labels, contain_gender_bias_encode)
        bias_labels = [kc[i]['bias'] for i in range(len(kc))] 
        if self.binary:
            bias_weight = self._calculate_class_weight(bias_labels, bias_encode, binary_label='bias') #idx2label로 대체
        else:
            bias_weight = self._calculate_class_weight(bias_labels, bias_encode)
        hate_labels = [kc[i]['hate'] for i in range(len(kc))] 
        if self.binary:
            hate_weight = self._calculate_class_weight(hate_labels, hate_encode, binary_label='hate')
        else:         
            hate_weight = self._calculate_class_weight(hate_labels, hate_encode)
        self.weights = {'contain_gender_bias':contain_gender_bias_weight,
                'bias':bias_weight, 
                'hate':hate_weight}

    def build_train_input_fn(self):
        # TODO kocodataset 중복으로 받아옴...... 
        dataset = kocodataset(self.max_length, self.tokenizer, title=self.title, evaluate=False, binary=self.binary)
        dataloader = DataLoader(dataset, batch_size=self.batch_size, shuffle=True, num_workers=4, collate_fn=pad)
        return dataloader
    def build_test_examples(self):
        dataset = kocodataset(self.max_length, self.tokenizer, title=self.title, evaluate=True, binary=self.binary)
        dataloader = DataLoader(dataset, batch_size=self.eval_batch_size, shuffle=False, num_workers=4, collate_fn=pad)
        return dataloader
    def build_test_input_fn(self):
        dataset = kocodataset(self.max_length, self.tokenizer, title=self.title, test=True, binary=self.binary)
        dataloader = DataLoader(dataset, batch_size=self.eval_batch_size, shuffle=False, num_workers=4, collate_fn=pad)
        return dataloader

    def build_infer_examples(self, infer_texts):
        examples = self._get_infer_examples(infer_texts)
        #dataloader = DataLoader(examples, batch_size=self.eval_batch_size, shuffle=False, num_workers=4, collate_fn=pad)
        return examples  # 리스트에 넣어서 한 뭉치씩 볼 수 있다

    def _get_infer_examples(self, infer_texts:list):
        def _convert_texts_to_examples(text):
            if isinstance(text, list):
                title = ' '.join(self.mecab.morphs(text[1]))
                text = ' '.join(self.mecab.morphs(text[0]))
                token = self.tokenizer(text=title,
                        text_pair=text,
                        padding='max_length', 
                        max_length=self.max_length)
            elif isinstance(text, str):
                text = ' '.join(self.mecab.morphs(text))
                token = self.tokenizer(text=text, 
                        padding='max_length', 
                        max_length=self.max_length)
            return token

        #examples = []

        inputs = {
            'input_ids': [],
            'attention_mask': [],
            'token_type_ids': []
        }
        targets = {
            'contain_gender_bias': [],
            'bias': [],
            'hate': []
        }
        seqlen = []
        for text in infer_texts:
            example = _convert_texts_to_examples(text)
            inputs['input_ids'].append(example.input_ids)
            inputs['attention_mask'].append(example.attention_mask)
            inputs['token_type_ids'].append(example.token_type_ids)

            targets['contain_gender_bias'] = -1
            targets['bias'] = -1
            targets['hate'] = -1

            seqlen.append(sum(example.attention_mask))

            #examples.append([text, inputs, targets, seqlen])
        '''
        examples = {'sentence': infer_texts,
                   'input':inputs,
                   'targets':targets,
                   'seqlen':seqlen}
        '''
        examples = [infer_texts, inputs, targets, seqlen]


        return examples


    def _calculate_class_weight(self, labels, label2idx, binary_label=None):
        """
        for weighted cross entropy loss to balance class weight
        """
        # 1. count elements numbers for each class
        # 2. calculate percentage of each class by num of elements
        label2idx = sorted(label2idx.items(), key=lambda idx: idx[1]) 
        count_dict = {str(label[0]).strip(): 0 for label in label2idx}

        for element in labels:
            if binary_label is not None:
                element = 'none' if element=='none' else binary_label
            count_dict[str(element)] += 1

        class_sample_count = torch.tensor([num for num in count_dict.values()])
        weight = 1. / class_sample_count.float()

        return weight * 10






'''
tokenizer = BertTokenizer.from_pretrained("beomi/kcbert-base")

dataset = kocodataset(tokenizer, title=True)
# for i in range(2):
#     print(dataset[i])

dataloader = DataLoader(dataset, batch_size=4,
                        shuffle=True,
                        num_workers=4, collate_fn=pad)
for i,batch in enumerate(dataloader):
    print(i)
    print(batch)
    break
'''
