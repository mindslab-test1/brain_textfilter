from __future__ import print_function
import grpc
import argparse
from collections import defaultdict

from proto.text_filter_pb2 import TextFilterRequest
from proto.text_filter_pb2_grpc import TextFilterStub

class TextFilterClient:
    def __init__(self, ip="localhost", port=35000):
        self.server_ip = ip
        self.server_port = port

        self.stub = TextFilterStub(
                grpc.insecure_channel(self.server_ip + ":" + str(self.server_port))
                )

    def request(self, input_text, input_title=''):
        myinput = TextFilterRequest()
        myinput.text = input_text
        myinput.subject = input_title
        
        results = self.stub.PredictTextLabel(myinput)
       
        outputs = defaultdict()
        for out in results.predictions:
            outputs[out.category] = [out.label, out.reliability]
        
        return outputs

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', type=str, default="localhost")
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--passage', type=str, default="PEF 테스트용 텍스트입니다. 원하는 텍스트를 입력하세요.")
    parser.add_argument('--title', type=str, default='')

    args = parser.parse_args()
    print(args.passage, args.title)
    pef = TextFilterClient(args.ip, args.port)
    results = pef.request(args.passage, args.title)
   
    print(f"Input text:{args.title} {args.passage}")
    print("Outputs:")
    for category, result in results.items():
        label, reliability = result
        print(f"{category}: {label}\ton reliability:{reliability}")

