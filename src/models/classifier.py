import torch
import torch.nn as nn
import torch.functional as F
import torch.utils.data as Data

from transformers import BertConfig, BertModel, BertTokenizer


class BertHateClassification(nn.Module):
    def __init__(self, config, model, class_weights):
        super(BertHateClassification, self).__init__()
        self.cfg = config
        self.config = config.pretrain
        self.num_labels = [2,3,3]  # if binary classification!

        binary_class_weight = class_weights['contain_gender_bias'] #torch.tensor([1., 30.])
        bias_class_weight = class_weights['bias']
        hate_class_weight = class_weights['hate']

        self.bert = model
        self.dropout = nn.Dropout(p=self.config.hidden_dropout_prob)
        self.classifier = nn.Linear(self.config.hidden_size , self.num_labels[0])
        self.bias_classifier = nn.Linear(self.config.hidden_size , self.num_labels[1])
        self.hate_classifier = nn.Linear(self.config.hidden_size , self.num_labels[2])
        self.softmax = nn.Softmax(dim=1)

        self.loss_fct1 = nn.CrossEntropyLoss(weight=binary_class_weight)
        #self.loss_fct2 = nn.CrossEntropyLoss(weight=multi_class_weight)
        self.loss_fct2 = nn.CrossEntropyLoss(weight=bias_class_weight)
        self.loss_fct3 = nn.CrossEntropyLoss(weight=hate_class_weight)
        self.relu = nn.GELU()

        self.init_weights()

    def init_weights(self):
        initrange = self.config.initializer_range
        #self.bert.weight.data.uniform_(-initrange, initrange)
        self.classifier.weight.data.uniform_(-initrange, initrange)
        self.classifier.bias.data.zero_()
        self.bias_classifier.weight.data.uniform_(-initrange, initrange)
        self.bias_classifier.bias.data.zero_()
        self.hate_classifier.weight.data.uniform_(-initrange, initrange)
        self.hate_classifier.bias.data.zero_()
    
    def save_pretrained(self, save_directory):
        self.bert.save_pretrained(save_directory)

    def forward(self,
                input_ids=None,
                attention_mask=None,
                token_type_ids=None,
                position_ids=None,
                head_mask=None,
                inputs_embeds=None,
                labels=None,
                output_attentions=None,
                output_hidden_states=None,
                mode=None
                ):

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
        )
        #print('outputs[0]:',outputs[0].shape, 'outputs[1]:',outputs[1].shape)
        # use mean->max -> concat
        apool = torch.mean(outputs[0],1)
        mpool, _ = torch.max(outputs[0],1)
        all_output = torch.cat((apool, mpool), 1)
        all_output = self.relu(all_output)

        # TODO cls 사용할지 mean->max 할지 조건문
        # use CLS token
        pooled_output = outputs[1]
        # do ReLU.. ? -> noReLU...?
        #pooled_output = self.relu(pooled_output)

        pooled_output = self.dropout(pooled_output) # use cls? mean max concat?

        # binary label classification
        logits1 = self.classifier(pooled_output)
        #logits1 = torch.sigmoid(logits1)
        # multi label classification
        logits2 = self.bias_classifier(pooled_output)
        logits3 = self.hate_classifier(pooled_output)
        logits = {
                'contain_gender_bias':logits1, 
                'bias':logits2, 
                'hate':logits3}

        outputs = (logits,) + outputs[2:]
        
        if mode == 'infer' or labels is None:
            logits = {k: self.softmax(v) for k, v in logits.items()}
            return logits
        #for gender_bias_detection
        loss1 = self.loss_fct1(logits1.view(-1, self.num_labels[0]), labels['contain_gender_bias'].view(-1))
        #loss1 = loss1.sum() / binary_class_weight[target]
        # for hate, bias detection
        loss2 = self.loss_fct2(logits2.view(-1, self.num_labels[1]), labels['bias'].view(-1))
        loss3 = self.loss_fct3(logits3.view(-1, self.num_labels[2]), labels['hate'].view(-1))
        if mode == None:
            loss = loss1 + loss2 + loss3
        elif mode == 'contain_gender_bias':
            loss = loss1
        elif mode == 'bias':
            loss = loss2
        elif mode == 'hate':
            loss = loss3
        outputs = (loss,) + outputs

        return outputs

'''
config = BertConfig()

Hate_finder = BertHateClassification(config)

## ==============================tokenizers ====================
tokenizer = BertTokenizer.from_pretrained("monologg/kobert")
tokenizer_koel = BertTokenizer.from_pretrained("monologg/koelectra-base-v3-discriminator")
tokenizer_kcbert = BertTokenizer.from_pretrained("beomi/kcbert-base")

## ================================================================

inputs = tokenizer("송중기 시대극은 믿고본다. 첫회 신선하고 좋았다.", return_tensors="pt")
y = {'contain_gender_bias':  torch.tensor([[1, 0]]), # 0=False 1 - True
     'bias': torch.tensor([[2]]), # gender : 0, others : 1, none: 2
     'hate': torch.tensor([[2]])} # hate : 0, offensive : 1, none: 2
print(inputs)

outputs = Hate_finder(**inputs, labels=y)
print('final output shape:: ', outputs)
'''
