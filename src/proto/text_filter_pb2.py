# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: text_filter.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='text_filter.proto',
  package='maum.brain.textfilter',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x11text_filter.proto\x12\x15maum.brain.textfilter\"2\n\x11TextFilterRequest\x12\x0c\n\x04text\x18\x01 \x01(\t\x12\x0f\n\x07subject\x18\x02 \x01(\t\"L\n\x12TextFilterResponse\x12\x36\n\x0bpredictions\x18\x01 \x03(\x0b\x32!.maum.brain.textfilter.Prediction\"B\n\nPrediction\x12\x10\n\x08\x63\x61tegory\x18\x01 \x01(\t\x12\r\n\x05label\x18\x02 \x01(\t\x12\x13\n\x0breliability\x18\x03 \x01(\x02\x32u\n\nTextFilter\x12g\n\x10PredictTextLabel\x12(.maum.brain.textfilter.TextFilterRequest\x1a).maum.brain.textfilter.TextFilterResponseb\x06proto3')
)




_TEXTFILTERREQUEST = _descriptor.Descriptor(
  name='TextFilterRequest',
  full_name='maum.brain.textfilter.TextFilterRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='text', full_name='maum.brain.textfilter.TextFilterRequest.text', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='subject', full_name='maum.brain.textfilter.TextFilterRequest.subject', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=44,
  serialized_end=94,
)


_TEXTFILTERRESPONSE = _descriptor.Descriptor(
  name='TextFilterResponse',
  full_name='maum.brain.textfilter.TextFilterResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='predictions', full_name='maum.brain.textfilter.TextFilterResponse.predictions', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=96,
  serialized_end=172,
)


_PREDICTION = _descriptor.Descriptor(
  name='Prediction',
  full_name='maum.brain.textfilter.Prediction',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='category', full_name='maum.brain.textfilter.Prediction.category', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='label', full_name='maum.brain.textfilter.Prediction.label', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='reliability', full_name='maum.brain.textfilter.Prediction.reliability', index=2,
      number=3, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=174,
  serialized_end=240,
)

_TEXTFILTERRESPONSE.fields_by_name['predictions'].message_type = _PREDICTION
DESCRIPTOR.message_types_by_name['TextFilterRequest'] = _TEXTFILTERREQUEST
DESCRIPTOR.message_types_by_name['TextFilterResponse'] = _TEXTFILTERRESPONSE
DESCRIPTOR.message_types_by_name['Prediction'] = _PREDICTION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

TextFilterRequest = _reflection.GeneratedProtocolMessageType('TextFilterRequest', (_message.Message,), dict(
  DESCRIPTOR = _TEXTFILTERREQUEST,
  __module__ = 'text_filter_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.textfilter.TextFilterRequest)
  ))
_sym_db.RegisterMessage(TextFilterRequest)

TextFilterResponse = _reflection.GeneratedProtocolMessageType('TextFilterResponse', (_message.Message,), dict(
  DESCRIPTOR = _TEXTFILTERRESPONSE,
  __module__ = 'text_filter_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.textfilter.TextFilterResponse)
  ))
_sym_db.RegisterMessage(TextFilterResponse)

Prediction = _reflection.GeneratedProtocolMessageType('Prediction', (_message.Message,), dict(
  DESCRIPTOR = _PREDICTION,
  __module__ = 'text_filter_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.textfilter.Prediction)
  ))
_sym_db.RegisterMessage(Prediction)



_TEXTFILTER = _descriptor.ServiceDescriptor(
  name='TextFilter',
  full_name='maum.brain.textfilter.TextFilter',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=242,
  serialized_end=359,
  methods=[
  _descriptor.MethodDescriptor(
    name='PredictTextLabel',
    full_name='maum.brain.textfilter.TextFilter.PredictTextLabel',
    index=0,
    containing_service=None,
    input_type=_TEXTFILTERREQUEST,
    output_type=_TEXTFILTERRESPONSE,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_TEXTFILTER)

DESCRIPTOR.services_by_name['TextFilter'] = _TEXTFILTER

# @@protoc_insertion_point(module_scope)
