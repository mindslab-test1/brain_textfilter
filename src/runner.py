import argparse, logging, shutil
from pathlib import Path
from datetime import datetime
from tqdm import tqdm
from omegaconf import OmegaConf
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import confusion_matrix, precision_score, recall_score 
from collections import defaultdict

from transformers import BertConfig, BertModel, BertTokenizer, get_linear_schedule_with_warmup
from MyDataset import KocoDataProcessor
from models.classifier import BertHateClassification

logger = logging.getLogger(__name__)

# a config file containing constants about paths & file names
CONST_CFG = 'cfg/const.yml'
const = OmegaConf.load(CONST_CFG)

class result_dict():
    """
    각 label 별 결과 저장하기 위한 dict
    """
    def __init__(self, idx2tag):
        self.res = {'Tags':[],'Y':[],'Y_hat':[],'results':[], 'confusion_matrix':[]}
        self.idx2tag = idx2tag

    def get_results(self, idx):
        tag = self.res['Tags'][idx]
        pred = self.idx2tag[self.res['Y_hat'][idx]]
        #self._get_conf(tag, pred)
        return tag, pred

    def get_conf(self):
        y_true = np.array(self.res['Y'])
        y_pred = np.array(self.res['Y_hat'])
        conf_mat = confusion_matrix(y_true, y_pred)
        self.res['confusion_matrix'].append(conf_mat)
        return conf_mat

# 1. Load data
# 2. Build input_fn
# 3. Build estimator/predictor
# 4. Run train/test/infer
# 5. Post process
class Trainer(object):
    def __init__(self, cfg):
        self.cfg = cfg
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

        self.tokenizer = BertTokenizer.from_pretrained(cfg.config_path, do_lower_case=False)
        self.data_processor = KocoDataProcessor(cfg, self.tokenizer, binary=False)  # binary config에 넣기
        self.train_iter = self.data_processor.build_train_input_fn()
        self.tag2idx = self.data_processor.label2idx
        self.idx2tag = self.data_processor.idx2label
        self.class_weights = self.data_processor.weights
        self.classes = ['contain_gender_bias','bias','hate']
        self.which_class = cfg.train.save_best_f1_class
	
        #badwords
        import json
        badwords_path = Path(cfg.train.badwords_file)
        with open(badwords_path, 'r', encoding='utf-8') as f:
            badwords = json.load(f)
        self.badword_list = [b.strip() for b in badwords['badwords']]

        #self.AddVocab = True if cfg.train.badwords_file is not None else False
        #if self.AddVocab and cfg.args.run == 'train':
        #    self._add_tokens(architecture, self.tokenizer, self.badword_list)

        architecture = BertModel.from_pretrained(cfg.config_path)
        self.model = BertHateClassification(cfg.model, architecture, self.class_weights)
        self.model.to(self.device)

        no_decay = ["bias", "LayerNorm.weight"]
        optimizer_grouped_parameters = [
            {
                "params": [p for n, p in self.model.named_parameters() if not any(nd in n for nd in no_decay)],
                "weight_decay": cfg.train.weight_decay
            },
            {
                "params": [p for n, p in self.model.named_parameters() if any(nd in n for nd in no_decay)],
                "weight_decay": 0.0
            }
        ]
        self.optimizer = optim.AdamW(optimizer_grouped_parameters, lr=cfg.train.learning_rate)

        self.epochs = cfg.train.epochs
        total_steps = len(self.train_iter) * self.epochs
        warmup_step = int(total_steps * cfg.train.warmup) if cfg.train.warmup else 0
        self.scheduler = get_linear_schedule_with_warmup(self.optimizer, num_warmup_steps=warmup_step, num_training_steps=total_steps)

    def is_bad(self, sentence):
        return True if any(b in sentence for b in self.badword_list) else False

    def _add_tokens(self, model, tokenizer, add_tokens):
        model_dir = Path(const.models_dir, self.cfg.model_name)
        num_newtokens = tokenizer.add_tokens(add_tokens)
        model.resize_token_embeddings(len(tokenizer))
        tokenizer.save_pretrained(model_dir)
        print(f"updated VOCAB was saved to {model_dir}")
        model.save_pretrained(model_dir)
        print(f"updated MODEL config was saved to {model_dir}")

    # Train
    def train(self):
        print('Start Train')
        print(f'Total Number of Parameters : {sum(p.nelement() for p in self.model.parameters()) / 2 ** 20:.3f}M\n')
        best_f1 = 0.0
        current_f1 = {label: 0 for label in self.classes}

        for epoch in range(1, self.epochs + 1):
            print('Epoch:', epoch, 'LR:', self.scheduler.get_last_lr())

            trainer._train(self.train_iter)

            print(f"============eval at epoch={epoch}===========")
            log_dir = Path(cfg.const.log_dir, cfg.model_name)
            log_dir.mkdir(parents=True, exist_ok=True)
            fname = log_dir / str(epoch)
            model_dir = Path(const.models_dir, cfg.model_name)  # Path(cfg.const.models_dir)
            mname = model_dir / str(epoch)
            print(fname)
            # fname = os.path.join(logdir, str(epoch))
            result = trainer.eval(fname)

            self.scheduler.step()

            if epoch != 0:
                torch.save(self.model.state_dict(), f"{mname}.pt")
                print(f"weight were saved to {mname}.pt")
                for label in self.classes:
                    current_f1[label] = result[label].res['results'][-1]
                if current_f1[self.which_class] > best_f1:
                    best_f1 = current_f1[self.which_class]
                    self.model.save_pretrained(model_dir)
                    bestname = model_dir/'pytorch_model.pt'
                    torch.save(self.model.state_dict(), f"{bestname}")
                    print(f"best model in {self.which_class} class weight were saved to {bestname}")


    def _train(self, train_iter):
        self.model.train()
        total_loss = 0.0
        for i, batch in enumerate(train_iter):
            self.optimizer.zero_grad()
            sentences, inputs, targets, seqlen = batch

            inputs= {k: torch.tensor(v, device=self.device) for k,v in inputs.items()}
            Targets ={k: torch.tensor(v, device=self.device) for k,v in targets.items()}# if k !='contain_gender_bias'}
            #gender_bias = torch.tensor(targets['contain_gender_bias'], dtype=torch.float, device=device)
            #Targets['contain_gender_bias'] = gender_bias

            loss, _ = self.model(**inputs, labels=Targets)
            total_loss += loss.item()
            loss.backward()
            self.optimizer.step()
            self.scheduler.step()

            if i == 0:
                print("=====sanity check======")
                print("words:", sentences[0])
                print("x:", inputs['input_ids'][0][:seqlen[0]].cpu().numpy())
                print("y:", targets['contain_gender_bias'][0], targets['bias'][0],targets['hate'][0])
                print("=======================")

            if i % 50 == 0:  # monitoring
                print(f"step: {i}, loss: {loss.item()}")

        avg_train_loss = total_loss / len(self.train_iter)
        print(" Average training loss : {0:0.2f}".format(avg_train_loss))


    def eval(self, f):
        self.model.eval()
        iterator = self.data_processor.build_test_examples()
        # unpack tag2idx
        contain_gender_bias_encode = self.tag2idx[0]
        bias_encode = self.tag2idx[1]
        hate_encode = self.tag2idx[2]

        # unpack idx2tag
        idx2contain_gender_bias = self.idx2tag[0]
        idx2bias = self.idx2tag[1]
        idx2hate = self.idx2tag[2]

        result = {'Words':[], 'contain_gender_bias':result_dict(idx2contain_gender_bias), 'bias':result_dict(idx2bias), 'hate':result_dict(idx2hate)}
        with torch.no_grad():
            for i, batch in enumerate(iterator):
                sentences, inputs, targets, seqlen = batch
                inputs= {k: torch.tensor(v, device=self.device) for k,v in inputs.items()}
                Targets ={k: torch.tensor(v, device=self.device) for k, v in targets.items()}

                loss, logits = self.model(**inputs, labels=Targets)  # loss, (logits,) + outputs[2:]

                # print('at eval, sentences:', sentences)  #: List[List * batch_size]
                result['Words'].extend(sentences)
                
                for k in targets.keys():
                    if k == 'contain_gender_bias':
                        tag2idx = contain_gender_bias_encode
                        idx2tag = idx2contain_gender_bias
                    elif k == 'bias':
                        tag2idx = bias_encode
                        idx2tag = idx2bias
                    elif k == 'hate':
                        tag2idx = hate_encode
                        idx2tag = idx2hate
                    tags = [idx2tag[int(lb.item())] for lb in Targets[k]]
                    # Tags[k].extend(tags)
                    result[k].res['Tags'].extend(tags)
                    y = [int(lb.item()) for lb in Targets[k]]
                    result[k].res['Y'].extend(y)
                    y_hat = np.argmax(logits[k].cpu().numpy(), axis=1).flatten().tolist()
                    result[k].res['Y_hat'].extend(y_hat)

        ## calc metric
        result_out = ""
        total_precision = 0
        total_recall = 0
        total_f1 = 0
        total_label = ""
        for label in targets.keys():
            print(label)
            y, y_h = result[label].res['Y'], result[label].res['Y_hat']
            y_true = np.array(y)
            y_pred = np.array(y_h)
            precision, recall, f1 = f1_score(y_pred, y_true)
            total_precision += precision
            total_recall += recall
            total_f1 += f1
            total_label += label[0].upper()

            result[label].res['results'].extend([precision, recall, f1])
            print("precision=%.4f" % precision)
            print("recall=%.4f" % recall)
            print("f1=%.4f" % f1)

        final = str(f) + ".%s_P%.4f_R%.4f_F%.4f" % (
        total_label, total_precision / 3, total_recall / 3, total_f1 / 3)

        with open(final, 'w', encoding='utf-8') as fout:
            for idx, sentences in enumerate(result['Words']):
                if isinstance(sentences, list):
                    title, comment = sentences[0], sentences[1]
                else:
                    title = "No title"
                    comment = sentences

                result_out += f'{title}\n{comment}\n'
                for label in targets.keys():
                    t, p = result[label].get_results(idx)
                    result_out += f'{label}:\ttrue_label:{t}\tpred_label:{p}\n'
            fout.write(f"{result_out}\n")
            fout.write("================================================\n")
            for label in targets.keys():
                precision, recall, f1 = result[label].res['results']
                conf_mat = np.array2string(result[label].get_conf())
                fout.write(label+'\n')
                fout.write(conf_mat+'\n')
                fout.write('\n')
                fout.write(f"precision={precision}\nrecall={recall}\nf1={f1}\n")

        return result

    def test(self):
        self.model.eval()

        #TODO 중복되는 파일이 너무 많다. 깔끔하게 정리하기
        MODEL_PATH = Path(self.cfg.config_path, 'pytorch_model.pt')
        self.model.load_state_dict(torch.load(MODEL_PATH, map_location=torch.device(self.device)))
        self.model.eval()
        
        iterator = self.data_processor.build_test_input_fn()
        # unpack tag2idx
        contain_gender_bias_encode = self.tag2idx[0]
        bias_encode = self.tag2idx[1]
        hate_encode = self.tag2idx[2]

        # unpack idx2tag
        idx2contain_gender_bias = self.idx2tag[0]
        idx2bias = self.idx2tag[1]
        idx2hate = self.idx2tag[2]

        Result = {'Words': [], 'contain_gender_bias': [], 'bias': [],
                  'hate': []}
        results = []
        with torch.no_grad():
            for j, batch in enumerate(iterator):
                sentences, inputs, targets, seqlen = batch
                inputs = {k: torch.tensor(v, device=self.device) for k, v in inputs.items()}
    
                logits = self.model(**inputs, labels=None)  # loss, (logits,) + outputs[2:]

                for i, sentence in enumerate(sentences):
                    if isinstance(sentence, list):
                        sentence = '\n'.join(sentence)
                    Result['Words'].append(sentence)
                    result = sentence+'\n'
                    for k in targets.keys():
                        if k == 'contain_gender_bias':
                            idx2tag = idx2contain_gender_bias
                        elif k == 'bias':
                            idx2tag = idx2bias
                        elif k == 'hate':
                            idx2tag = idx2hate
    
                        _, predicted = torch.max(logits[k][i], -1)
                        pred_label = idx2tag[predicted.item()]
                        probability = logits[k][i][predicted.item()]
                        Result[k].append(pred_label)
                        result += 'label: {0}\tpredict: {1}\tprobability: {2:0.3f}\n'.format(k, pred_label, probability)
                    results.append(result)

        # for kaggle leaderboard
        import pandas as pd
        log_dir = Path(cfg.const.log_dir, cfg.model_name)
        log_dir.mkdir(parents=True, exist_ok=True)

        for k in self.classes:
            file_name = str(log_dir/  (k + '_submit.csv'))
            #with open(file_name, 'w', newline='') as file:
            in_data = {'comments':Result['Words'], 'label':Result[k]}
            df = pd.DataFrame(in_data)
            df.to_csv(file_name, index=False, encoding='utf-8')
        return results

    def infer(self, infer_texts):
        self.model.eval()
        #TODO 중복되는 파일이 너무 많다. 깔끔하게 정리하기
        MODEL_PATH = Path(self.cfg.config_path, 'pytorch_model.pt')
        self.model.load_state_dict(torch.load(MODEL_PATH, map_location=torch.device(self.device)))
        self.model.eval()
        iterator = self.data_processor.build_infer_examples(infer_texts)
        # unpack tag2idx
        contain_gender_bias_encode = self.tag2idx[0]
        bias_encode = self.tag2idx[1]
        hate_encode = self.tag2idx[2]

        # unpack idx2tag
        idx2contain_gender_bias = self.idx2tag[0]
        idx2bias = self.idx2tag[1]
        idx2hate = self.idx2tag[2]

        with torch.no_grad():
            sentences, inputs, targets, seqlen = iterator
            inputs = {k: torch.tensor(v, device=self.device) for k, v in inputs.items()}

            logits = self.model(**inputs, labels=None)  # loss, (logits,) + outputs[2:]

            results = []
            Result = defaultdict()
            for i, sentence in enumerate(sentences):
                for k in targets.keys():
                    _, predicted = torch.max(logits[k][i], -1)
                    
                    if k == 'contain_gender_bias':
                        idx2tag = idx2contain_gender_bias
                        continue
                    elif k == 'bias':
                        idx2tag = idx2bias
                    elif k == 'hate':
                        idx2tag = idx2hate

                    if k == 'hate' and any(b in sentence for b in self.badword_list):
                        pred_label = 'hate'
                        probability = 1.
                    else:
                        pred_label = idx2tag[predicted.item()]
                        probability = logits[k][i][predicted.item()].item()
                    
                    Result[k] = [pred_label, probability]
                results.append([sentence, Result])
        return results

def f1_score(y_pred:np.array, y_true:np.array):
    """
    y_pred: (np.array)
    y_true: (np.array)
    precision: (float)
    recall: (float)
    f1 : (float)
    """
    assert len(y_pred) == len(y_true), f'check len, y_pred:{len(y_pred)} y_true:{len(y_true)}'
    
    ## calc metric
    conf_mat = confusion_matrix(y_true, y_pred)
    # ? confution matrix도 같이 리턴할지..?
    print(conf_mat)

    precision = precision_score(y_true, y_pred, average='weighted')
    recall = recall_score(y_true, y_pred, average='weighted')

    if precision + recall == 0:
        f1 = 0
    else:
        f1 = 2 * (precision * recall) / (precision + recall)

    return precision, recall, f1


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    # Arguments for running.
    parser.add_argument('--run', type=str, required=True, choices=['train', 'test', 'infer'])
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    # Configuration files for training
    parser.add_argument('--cfg', type=str)
    parser.add_argument('--model_cfg', type=str)

    # for test/inference. config files will be loaded from the model_path
    parser.add_argument('--model_path', type=str)
    parser.add_argument('--infer_text', type=str, dest='infer_texts', action='append')

    args = parser.parse_args()
    return args

def setup_logger(cfg):
    log_file_name = '{}_{}.log'.format(datetime.now().strftime('%y%m%d_%H%M%S'), cfg.args.run)
    log_file_path = Path(cfg.const.log_dir, cfg.model_name, log_file_name)
    log_file_path.parent.mkdir(parents=True, exist_ok=True)

    logging.basicConfig(level=logging.DEBUG)
    formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(asctime)s] %(message)s')

    # create file handler
    fh = logging.FileHandler(str(log_file_path))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    # create stderr handler
    ch = logging.StreamHandler()    # stderr by default
    ch.setLevel(cfg.args.log_level)
    ch.setFormatter(formatter)
    # because 'tensorflow' logger output log to stderr, setting additional stream handler is not necessary.

    handlers = [fh, ch]
    logging.getLogger('').handlers = handlers
    logging.getLogger('tensorflow').handlers = []
    # logger.handlers = handlers

# Setup configuration
def setup_config(args):
    # Set model_dir_path
    if args.run == 'train':
        cfg = OmegaConf.load(args.cfg)
        model_dir_path = Path(const.models_dir, cfg.model_name)
        model_dir_path.mkdir(parents=True, exist_ok=True)
    else:
        model_path = Path(args.model_path)
        model_dir_path = model_path if model_path.is_dir() else model_path.parent

    # paths for essential files under the model dir
    cfg_file = str(model_dir_path.joinpath(const.run_cfg))
    model_cfg_file = str(model_dir_path.joinpath(const.model_cfg))
    vocab_file = str(model_dir_path.joinpath(const.vocab_file))
    cfg_json_file = str(model_dir_path.joinpath('config.json'))

    # Pack & store essential configurations into the model dir.
    if args.run == 'train':
        shutil.copyfile(args.cfg, cfg_file)
        shutil.copyfile(args.model_cfg, model_cfg_file)
        shutil.copyfile(cfg.train.vocab_file, vocab_file)
        config_path = Path(cfg.train.pretrained_ckpt)
        config_path = config_path if config_path.is_dir() else config_path.parent
        json_path = config_path / 'config.json'
        shutil.copyfile(str(json_path), cfg_json_file)

    # Load configurations from the model dir
    cfg = OmegaConf.load(cfg_file)
    cfg.model = OmegaConf.load(model_cfg_file)
    cfg.const = const
    cfg.args = vars(args)

    # Set a proper checkpoint path
    # train: latest > pretrained
    # infer: model_path > latest

    latest_checkpoint = []
    latest_ckpt = str(model_dir_path)  # ==================================================================================check!!

    if args.run == 'train':
        config_path = Path(cfg.train.pretrained_ckpt)
        if config_path.exists():
            # if pre-trained model file exist.
            model_ckpt = config_path if config_path.is_dir() else str(config_path.parent)
        else:
            # if use huggingface model
            model_ckpt = str(cfg.train.pretrained_ckpt)
        #model_ckpt = cfg.train.pretrained_ckpt if latest_ckpt is None else latest_ckpt
    else:
        config_path = Path(cfg.args.model_path)
        model_ckpt = str(config_path) if config_path.is_dir() else str(config_path.parent)

    cfg.config_path = model_ckpt

    return cfg


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    args = parse_args()
    cfg = setup_config(args)

    # set seed
    seed_val = cfg.seed_bucket
    torch.manual_seed(seed_val)
    torch.cuda.manual_seed(seed_val)

    trainer = Trainer(cfg)
    if args.run == 'train':
        trainer.train()

    # test
    elif args.run == 'test':
        results = trainer.test()
        for result in results:
            print(result)

    # Infer
    elif args.run == 'infer':
        infer_texts = cfg.args.infer_texts
        outputs = trainer.infer(infer_texts)
        for output in outputs:
            sentence, results = output
            print(sentence)
            for category, predection in results.items():
                label, rel = predection
                print(category, label, rel)

    print("Done running")

