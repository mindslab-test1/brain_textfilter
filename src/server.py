# MindsLab Inc.
# Text Filter Engine.
#
# ver 1.1.0
import time
import grpc
import logging
from concurrent import futures
import argparse

import torch

from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path

from runner import setup_config, setup_logger, logger, Trainer

from proto.text_filter_pb2 import TextFilterResponse, Prediction
from proto.text_filter_pb2_grpc import TextFilterServicer, add_TextFilterServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class TextFilter(TextFilterServicer):
    def __init__(self, cfg):
        self.runner = Trainer(cfg)

        logger.info('Warming up the GPU with a dummy data')
        _ = self.runner.infer(['Dummy text for warmup'])
        logger.info('Done warming up')

    def PredictTextLabel(self, msg, context):
        '''
        cfg.
        runner.infer()
        '''
        if msg.subject !=  '':
            infer_msg = [msg.subject, msg.text]
        else:
            infer_msg = msg.text
        results = self.runner.infer([infer_msg])
        logger.debug(results)

        outputs = self._postprocess(results)

        return outputs

    def _postprocess(self, outputs):
        predictions = []
        for output in outputs:
            sentence, results = output
            for category, predection in results.items():
                label, reliability = predection
                predictions.append(
                        Prediction(
                            category = category, 
                            label = label, 
                            reliability = reliability)
                        )
        _outputs = TextFilterResponse(
                predictions = predictions
                )

        return _outputs


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_path', type=str, required=True)
    parser.add_argument('--log_level', type=str, default='INFO', choices=['INFO', 'DEBUG'])

    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    args.run = 'serve'

    cfg = setup_config(args)
    setup_logger(cfg)

    logger.info('Initializing text filter engine')
    hatespeechfilter = TextFilter(cfg)

    logger.info('Building grpc server')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_TextFilterServicer_to_server(hatespeechfilter, server)

    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logger.info('PEF Server starting at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('Shutting down the server')
        server.stop(0)
